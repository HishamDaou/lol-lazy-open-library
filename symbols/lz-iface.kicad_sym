(kicad_symbol_lib (version 20201005) (generator kicad_symbol_editor)
  (symbol "lz-iface:CP2102N-A02-GQFN28" (in_bom yes) (on_board yes)
    (property "Reference" "U" (id 0) (at -10.16 25.4 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Value" "CP2102N-A02-GQFN28" (id 1) (at 0 -24.13 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Footprint" "QFN:QFN29P50_500X500X80L55X25T325N" (id 2) (at 13.97 -13.97 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "SILAB_SP2102N.pdf" (id 3) (at 13.97 -13.97 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Vendor" "https://eu.mouser.com/ProductDetail/Silicon-Labs/CP2102N-A02-GQFN28?qs=u16ybLDytRaSBkSkyjYFiA%3D%3D" (id 4) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Part" "CP2102N-A02-GQFN28" (id 5) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Manufacturer" "Silicon Laboratories" (id 6) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Category" "USB Interface" (id 7) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Package" "QFN-28" (id 8) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Type" "Bridge, USB to UART" (id 9) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Standard" "USB 2.0" (id 10) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Speed" "Full Speed" (id 11) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Data Rate" "3 Mb/s" (id 12) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Supply Voltage" "3/3.6 V" (id 13) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Operating Temperature" "-40/+85 C" (id 14) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Supply Current" "13.7 mA" (id 15) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_description" "USB Interface IC USBXpress - USB to UART Bridge QFN28" (id 16) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "CP2102N-A02-GQFN28_0_1"
      (rectangle (start -10.16 24.13) (end 10.16 -22.86)
        (stroke (width 0.001)) (fill (type background))
      )
    )
    (symbol "CP2102N-A02-GQFN28_0_0"
      (pin input line (at 12.7 -8.89 180) (length 2.54)
        (name "DCD" (effects (font (size 1.27 1.27))))
        (number "1" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 -11.43 180) (length 2.54)
        (name "RI/CLK" (effects (font (size 1.27 1.27))))
        (number "2" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 12.7 -20.32 180) (length 2.54)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "3" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 15.24 180) (length 2.54)
        (name "D+" (effects (font (size 1.27 1.27))))
        (number "4" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 12.7 180) (length 2.54)
        (name "D-" (effects (font (size 1.27 1.27))))
        (number "5" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -12.7 21.59 0) (length 2.54)
        (name "VDD" (effects (font (size 1.27 1.27))))
        (number "6" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -12.7 19.05 0) (length 2.54)
        (name "VREGIN" (effects (font (size 1.27 1.27))))
        (number "7" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -12.7 15.24 0) (length 2.54)
        (name "VBUS" (effects (font (size 1.27 1.27))))
        (number "8" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -12.7 11.43 0) (length 2.54)
        (name "~RST" (effects (font (size 1.27 1.27))))
        (number "9" (effects (font (size 1.27 1.27))))
      )
      (pin unconnected line (at -12.7 -20.32 0) (length 2.54)
        (name "NC" (effects (font (size 1.27 1.27))))
        (number "10" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at 12.7 21.59 180) (length 2.54)
        (name "~SUSPEND" (effects (font (size 1.27 1.27))))
        (number "11" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at 12.7 19.05 180) (length 2.54)
        (name "SUSPEND" (effects (font (size 1.27 1.27))))
        (number "12" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at -12.7 -11.43 0) (length 2.54)
        (name "CHREN" (effects (font (size 1.27 1.27))))
        (number "13" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at -12.7 -13.97 0) (length 2.54)
        (name "CHR1" (effects (font (size 1.27 1.27))))
        (number "14" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at -12.7 -16.51 0) (length 2.54)
        (name "CHR0" (effects (font (size 1.27 1.27))))
        (number "15" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -12.7 0 0) (length 2.54)
        (name "GPIO.3/WAKEUP" (effects (font (size 1.27 1.27))))
        (number "16" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -12.7 2.54 0) (length 2.54)
        (name "GPIO.2/RS485" (effects (font (size 1.27 1.27))))
        (number "17" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -12.7 5.08 0) (length 2.54)
        (name "GPIO.1/RXT" (effects (font (size 1.27 1.27))))
        (number "18" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -12.7 7.62 0) (length 2.54)
        (name "GPIO.0/TXT" (effects (font (size 1.27 1.27))))
        (number "19" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -12.7 -7.62 0) (length 2.54)
        (name "GPIO.6" (effects (font (size 1.27 1.27))))
        (number "20" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -12.7 -5.08 0) (length 2.54)
        (name "GPIO.5" (effects (font (size 1.27 1.27))))
        (number "21" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -12.7 -2.54 0) (length 2.54)
        (name "GPIO.4" (effects (font (size 1.27 1.27))))
        (number "22" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at 12.7 1.27 180) (length 2.54)
        (name "CTS" (effects (font (size 1.27 1.27))))
        (number "23" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at 12.7 -1.27 180) (length 2.54)
        (name "RTS" (effects (font (size 1.27 1.27))))
        (number "24" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at 12.7 5.08 180) (length 2.54)
        (name "RXD" (effects (font (size 1.27 1.27))))
        (number "25" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at 12.7 7.62 180) (length 2.54)
        (name "TXD" (effects (font (size 1.27 1.27))))
        (number "26" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at 12.7 -3.81 180) (length 2.54)
        (name "DSR" (effects (font (size 1.27 1.27))))
        (number "27" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at 12.7 -6.35 180) (length 2.54)
        (name "DTR" (effects (font (size 1.27 1.27))))
        (number "28" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 12.7 -16.51 180) (length 2.54)
        (name "EPAD" (effects (font (size 1.27 1.27))))
        (number "29" (effects (font (size 1.27 1.27))))
      )
    )
  )
  (symbol "lz-iface:LAN8742A" (in_bom yes) (on_board yes)
    (property "Reference" "U" (id 0) (at -17.78 25.4 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Value" "LAN8742A" (id 1) (at 0 -25.4 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Footprint" "QFN:QFN25P50_400X400X100L40X24T250N" (id 2) (at 48.26 1.27 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "MICROCHIP_LAN8742_00001989A.pdf" (id 3) (at 48.26 1.27 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Manufacturer" "Microchip" (id 4) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Part" "LAN8742A-CZ-TR" (id 5) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Vendor" "https://eu.mouser.com/ProductDetail/Microchip-Technology/LAN8742A-CZ-TR?qs=6qKXPk0vzx0Y6uPCt7MXWw==" (id 6) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_description" "Ethernet PHY - 10/100 RMII" (id 7) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "LAN8742A_0_1"
      (rectangle (start -19.05 24.13) (end 19.05 -24.13)
        (stroke (width 0.001)) (fill (type background))
      )
    )
    (symbol "LAN8742A_0_0"
      (pin power_in line (at -21.59 19.05 0) (length 2.54)
        (name "VDD2A" (effects (font (size 1.27 1.27))))
        (number "1" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 21.59 -2.54 180) (length 2.54)
        (name "LED2/nINTSEL" (effects (font (size 1.27 1.27))))
        (number "2" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 21.59 0 180) (length 2.54)
        (name "LED1/REGOFF" (effects (font (size 1.27 1.27))))
        (number "3" (effects (font (size 1.27 1.27))))
      )
      (pin input clock (at 21.59 -10.16 180) (length 2.54)
        (name "XTAL2" (effects (font (size 1.27 1.27))))
        (number "4" (effects (font (size 1.27 1.27))))
      )
      (pin input clock (at 21.59 -7.62 180) (length 2.54)
        (name "XTAL1" (effects (font (size 1.27 1.27))))
        (number "5" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -21.59 8.89 0) (length 2.54)
        (name "VDDCR" (effects (font (size 1.27 1.27))))
        (number "6" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -21.59 -15.24 0) (length 2.54)
        (name "RXD1/MODE1" (effects (font (size 1.27 1.27))))
        (number "7" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -21.59 -12.7 0) (length 2.54)
        (name "RXD0/MODE0" (effects (font (size 1.27 1.27))))
        (number "8" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -21.59 16.51 0) (length 2.54)
        (name "VDDIO" (effects (font (size 1.27 1.27))))
        (number "9" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at -21.59 -17.78 0) (length 2.54)
        (name "RXER/PHYAD0" (effects (font (size 1.27 1.27))))
        (number "10" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at -21.59 -21.59 0) (length 2.54)
        (name "CRS_DIV/MODE2" (effects (font (size 1.27 1.27))))
        (number "11" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -21.59 2.54 0) (length 2.54)
        (name "MDIO" (effects (font (size 1.27 1.27))))
        (number "12" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -21.59 5.08 0) (length 2.54)
        (name "MDC" (effects (font (size 1.27 1.27))))
        (number "13" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -21.59 0 0) (length 2.54)
        (name "nINT/REFCLKO" (effects (font (size 1.27 1.27))))
        (number "14" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -21.59 12.7 0) (length 2.54)
        (name "nRST" (effects (font (size 1.27 1.27))))
        (number "15" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -21.59 -3.81 0) (length 2.54)
        (name "TXEN" (effects (font (size 1.27 1.27))))
        (number "16" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at -21.59 -6.35 0) (length 2.54)
        (name "TXD0" (effects (font (size 1.27 1.27))))
        (number "17" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at -21.59 -8.89 0) (length 2.54)
        (name "TXD1" (effects (font (size 1.27 1.27))))
        (number "18" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -21.59 21.59 0) (length 2.54)
        (name "VDD1A" (effects (font (size 1.27 1.27))))
        (number "19" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at 21.59 19.05 180) (length 2.54)
        (name "TXN" (effects (font (size 1.27 1.27))))
        (number "20" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at 21.59 21.59 180) (length 2.54)
        (name "TXP" (effects (font (size 1.27 1.27))))
        (number "21" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at 21.59 6.35 180) (length 2.54)
        (name "RXN" (effects (font (size 1.27 1.27))))
        (number "22" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at 21.59 8.89 180) (length 2.54)
        (name "RXP" (effects (font (size 1.27 1.27))))
        (number "23" (effects (font (size 1.27 1.27))))
      )
      (pin passive line (at 21.59 -16.51 180) (length 2.54)
        (name "RBIAS" (effects (font (size 1.27 1.27))))
        (number "24" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 21.59 -21.59 180) (length 2.54)
        (name "EPAD" (effects (font (size 1.27 1.27))))
        (number "25" (effects (font (size 1.27 1.27))))
      )
    )
  )
  (symbol "lz-iface:MCP2200T-I_SS" (in_bom yes) (on_board yes)
    (property "Reference" "U" (id 0) (at -8.89 19.05 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Value" "MCP2200T-I_SS" (id 1) (at 0 -20.32 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Footprint" "SOP:SOIC20P127_1280X1030X265L83X41N" (id 2) (at -6.35 -22.86 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "MICROCHIP_MCP2200.pdf" (id 3) (at -6.35 -22.86 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Vendor" "https://eu.mouser.com/ProductDetail/Microchip-Technology/MCP2200T-I-SS?qs=kI7nsUnms46a9nk6uW8cGw%3D%3D" (id 4) (at -6.35 -22.86 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Part" "MCP2200T-I/SS" (id 5) (at -6.35 -22.86 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Manufacturer" "Microchip" (id 6) (at -6.35 -22.86 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Package" "SSOP-20" (id 7) (at -6.35 -22.86 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Product" "USB Controller" (id 8) (at -6.35 -22.86 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Type" "Bridge, USB to UART" (id 9) (at -6.35 -22.86 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Standard" "USB 2.0" (id 10) (at -6.35 -22.86 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Data Rate" "12 Mb/s" (id 11) (at -6.35 -22.86 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Operating Temperature" "-40/+85 C" (id 12) (at -6.35 -22.86 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_description" "USB Interface IC USB-to-UART Protocol Converter w/ GPIO" (id 13) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "MCP2200T-I_SS_0_1"
      (rectangle (start -10.16 17.78) (end 10.16 -19.05)
        (stroke (width 0.001)) (fill (type background))
      )
    )
    (symbol "MCP2200T-I_SS_0_0"
      (pin power_in line (at -12.7 15.24 0) (length 2.54)
        (name "VDD" (effects (font (size 1.27 1.27))))
        (number "1" (effects (font (size 1.27 1.27))))
      )
      (pin passive line (at -12.7 7.62 0) (length 2.54)
        (name "OSC1" (effects (font (size 1.27 1.27))))
        (number "2" (effects (font (size 1.27 1.27))))
      )
      (pin passive line (at -12.7 5.08 0) (length 2.54)
        (name "OSC2" (effects (font (size 1.27 1.27))))
        (number "3" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -12.7 11.43 0) (length 2.54)
        (name "~RST" (effects (font (size 1.27 1.27))))
        (number "4" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 -2.54 180) (length 2.54)
        (name "GP7/TXLED" (effects (font (size 1.27 1.27))))
        (number "5" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 0 180) (length 2.54)
        (name "GP6/RXLED" (effects (font (size 1.27 1.27))))
        (number "6" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 2.54 180) (length 2.54)
        (name "GP5" (effects (font (size 1.27 1.27))))
        (number "7" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 5.08 180) (length 2.54)
        (name "GP4" (effects (font (size 1.27 1.27))))
        (number "8" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 7.62 180) (length 2.54)
        (name "GP3" (effects (font (size 1.27 1.27))))
        (number "9" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at -12.7 1.27 0) (length 2.54)
        (name "TX" (effects (font (size 1.27 1.27))))
        (number "10" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -12.7 -3.81 0) (length 2.54)
        (name "~RTS" (effects (font (size 1.27 1.27))))
        (number "11" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -12.7 -1.27 0) (length 2.54)
        (name "RX" (effects (font (size 1.27 1.27))))
        (number "12" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -12.7 -6.35 0) (length 2.54)
        (name "~CTS" (effects (font (size 1.27 1.27))))
        (number "13" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 10.16 180) (length 2.54)
        (name "GP2" (effects (font (size 1.27 1.27))))
        (number "14" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 12.7 180) (length 2.54)
        (name "GP1/USBCFG" (effects (font (size 1.27 1.27))))
        (number "15" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 15.24 180) (length 2.54)
        (name "GP0/SSPND" (effects (font (size 1.27 1.27))))
        (number "16" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 12.7 -12.7 180) (length 2.54)
        (name "VUSB" (effects (font (size 1.27 1.27))))
        (number "17" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 -8.89 180) (length 2.54)
        (name "D-" (effects (font (size 1.27 1.27))))
        (number "18" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 -6.35 180) (length 2.54)
        (name "D+" (effects (font (size 1.27 1.27))))
        (number "19" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 12.7 -16.51 180) (length 2.54)
        (name "VSS" (effects (font (size 1.27 1.27))))
        (number "20" (effects (font (size 1.27 1.27))))
      )
    )
  )
  (symbol "lz-iface:PALF2012" (in_bom yes) (on_board yes)
    (property "Reference" "U" (id 0) (at -16.51 43.18 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Value" "PALF2012" (id 1) (at 0 -41.91 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Footprint" "QFP:QFP48P50_900X900X160L60X22N" (id 2) (at -3.81 3.81 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "PROFICHIP_VPC3_S.pdf" (id 3) (at -3.81 3.81 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Part" "PALF2012 VPC3+S" (id 4) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Manufacturer" "Profichip" (id 5) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Vendor" "https://profichipusa.com/collections/asics/products/palf2012-vpc3-s" (id 6) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Package" "LQFP48" (id 7) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Protocol" "Profibus DP-V0, Profibus DP-V1, Profibus DP-V2" (id 8) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Data Rate" "12 Mbit/s max" (id 9) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_description" "PROFIBUS-DP V1/V2 Slave Controller with serial interface. SPC3 and DPC31 compatible." (id 10) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "PALF2012_0_1"
      (rectangle (start -17.78 41.91) (end 17.78 -40.64)
        (stroke (width 0.001)) (fill (type background))
      )
    )
    (symbol "PALF2012_0_0"
      (pin bidirectional line (at -20.32 -1.27 0) (length 2.54)
        (name "AB7/SPI_MOSI" (effects (font (size 1.27 1.27))))
        (number "1" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -20.32 -8.89 0) (length 2.54)
        (name "AB10/SPI_CPOL" (effects (font (size 1.27 1.27))))
        (number "2" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 -11.43 0) (length 2.54)
        (name "XCS/AB11/SPI_XSS" (effects (font (size 1.27 1.27))))
        (number "3" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 -35.56 0) (length 2.54)
        (name "DIVIDER" (effects (font (size 1.27 1.27))))
        (number "4" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -20.32 27.94 0) (length 2.54)
        (name "XTEST0" (effects (font (size 1.27 1.27))))
        (number "5" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -20.32 39.37 0) (length 2.54)
        (name "VCC" (effects (font (size 1.27 1.27))))
        (number "6" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 20.32 -30.48 180) (length 2.54)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "7" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at 20.32 -15.24 180) (length 2.54)
        (name "CLKOUT" (effects (font (size 1.27 1.27))))
        (number "8" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 -33.02 0) (length 2.54)
        (name "SERMODE" (effects (font (size 1.27 1.27))))
        (number "9" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at 20.32 -11.43 180) (length 2.54)
        (name "CLK" (effects (font (size 1.27 1.27))))
        (number "10" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at 20.32 5.08 180) (length 2.54)
        (name "XDATAEXCH" (effects (font (size 1.27 1.27))))
        (number "11" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 21.59 0) (length 2.54)
        (name "RESET" (effects (font (size 1.27 1.27))))
        (number "12" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at 20.32 16.51 180) (length 2.54)
        (name "TXD" (effects (font (size 1.27 1.27))))
        (number "13" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 20.32 11.43 180) (length 2.54)
        (name "RTS" (effects (font (size 1.27 1.27))))
        (number "14" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at 20.32 13.97 180) (length 2.54)
        (name "RXD" (effects (font (size 1.27 1.27))))
        (number "15" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 20.32 8.89 180) (length 2.54)
        (name "XCTS" (effects (font (size 1.27 1.27))))
        (number "16" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at -20.32 -26.67 0) (length 2.54)
        (name "INT" (effects (font (size 1.27 1.27))))
        (number "17" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -20.32 36.83 0) (length 2.54)
        (name "VCC" (effects (font (size 1.27 1.27))))
        (number "18" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 20.32 -33.02 180) (length 2.54)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "19" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 20.32 21.59 180) (length 2.54)
        (name "DB7" (effects (font (size 1.27 1.27))))
        (number "20" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 20.32 34.29 180) (length 2.54)
        (name "DB2" (effects (font (size 1.27 1.27))))
        (number "21" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 20.32 29.21 180) (length 2.54)
        (name "DB4" (effects (font (size 1.27 1.27))))
        (number "22" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 20.32 24.13 180) (length 2.54)
        (name "DB6" (effects (font (size 1.27 1.27))))
        (number "23" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 20.32 26.67 180) (length 2.54)
        (name "DB5" (effects (font (size 1.27 1.27))))
        (number "24" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 20.32 31.75 180) (length 2.54)
        (name "DB3" (effects (font (size 1.27 1.27))))
        (number "25" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 20.32 39.37 180) (length 2.54)
        (name "DB0" (effects (font (size 1.27 1.27))))
        (number "26" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 20.32 36.83 180) (length 2.54)
        (name "DB1" (effects (font (size 1.27 1.27))))
        (number "27" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 -38.1 0) (length 2.54)
        (name "MOT/XINT" (effects (font (size 1.27 1.27))))
        (number "28" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -20.32 25.4 0) (length 2.54)
        (name "XTEST1" (effects (font (size 1.27 1.27))))
        (number "29" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -20.32 34.29 0) (length 2.54)
        (name "VCC" (effects (font (size 1.27 1.27))))
        (number "30" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 20.32 -35.56 180) (length 2.54)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "31" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -20.32 -17.78 0) (length 2.54)
        (name "XWR/E_CLOCK/AB11" (effects (font (size 1.27 1.27))))
        (number "32" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 -30.48 0) (length 2.54)
        (name "MODE" (effects (font (size 1.27 1.27))))
        (number "33" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 -22.86 0) (length 2.54)
        (name "XRD/R_W" (effects (font (size 1.27 1.27))))
        (number "34" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 -13.97 0) (length 2.54)
        (name "ALE/AS/AB11" (effects (font (size 1.27 1.27))))
        (number "35" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 16.51 0) (length 2.54)
        (name "AB0/I2C_SA0" (effects (font (size 1.27 1.27))))
        (number "36" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 11.43 0) (length 2.54)
        (name "AB2/I2C_SA2" (effects (font (size 1.27 1.27))))
        (number "37" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 8.89 0) (length 2.54)
        (name "AB3/I2C_SA3" (effects (font (size 1.27 1.27))))
        (number "38" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 13.97 0) (length 2.54)
        (name "AB1/I2C_SA1" (effects (font (size 1.27 1.27))))
        (number "39" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at 20.32 1.27 180) (length 2.54)
        (name "SYNC" (effects (font (size 1.27 1.27))))
        (number "40" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 6.35 0) (length 2.54)
        (name "AB4/I2C_SA4" (effects (font (size 1.27 1.27))))
        (number "41" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -20.32 31.75 0) (length 2.54)
        (name "VCC" (effects (font (size 1.27 1.27))))
        (number "42" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 20.32 -38.1 180) (length 2.54)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "43" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 -6.35 0) (length 2.54)
        (name "AB9/SPI_CPHA" (effects (font (size 1.27 1.27))))
        (number "44" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 1.27 0) (length 2.54)
        (name "AB6/I2C_SA6" (effects (font (size 1.27 1.27))))
        (number "45" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 3.81 0) (length 2.54)
        (name "AB5/I2C_SA5" (effects (font (size 1.27 1.27))))
        (number "46" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -20.32 -20.32 0) (length 2.54)
        (name "XREADY/DTACK/SPI_MISO/I2C_SDA" (effects (font (size 1.27 1.27))))
        (number "47" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -20.32 -3.81 0) (length 2.54)
        (name "AB8/SPI_SCK/I2C_SCK" (effects (font (size 1.27 1.27))))
        (number "48" (effects (font (size 1.27 1.27))))
      )
    )
  )
  (symbol "lz-iface:SN65HVD75DRBR" (in_bom yes) (on_board yes)
    (property "Reference" "U" (id 0) (at -3.81 10.16 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Value" "SN65HVD75DRBR" (id 1) (at 0 -10.16 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Footprint" "SON:SON9P65_300X300X100L40X30T240X165N" (id 2) (at 0 -12.7 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "TI_SN65HVD75.pdf" (id 3) (at 0 -12.7 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Vendor" "https://eu.mouser.com/ProductDetail/Texas-Instruments/SN65HVD75DRBR?qs=uuvYgmlZVOv4dJjw8vOY%2FA%3D%3D" (id 4) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Part" "SN65HVD75DRBR" (id 5) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Manufacturer" "Texas Instruments" (id 6) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Duplex" "Half" (id 7) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Supply Voltage" "3.3 V" (id 8) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Sginalling Max" "20 Mbps" (id 9) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "IEC 61000" "12 Kv" (id 10) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_description" "RS-485 Interface IC 3.3V-Supply RS-485 w/ IEC ESD Protect" (id 11) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "SN65HVD75DRBR_0_1"
      (rectangle (start -5.08 8.89) (end 5.08 -8.89)
        (stroke (width 0.001)) (fill (type background))
      )
    )
    (symbol "SN65HVD75DRBR_0_0"
      (pin output line (at -7.62 2.54 0) (length 2.54)
        (name "R" (effects (font (size 1.27 1.27))))
        (number "1" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -7.62 -3.81 0) (length 2.54)
        (name "~RE" (effects (font (size 1.27 1.27))))
        (number "2" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -7.62 -6.35 0) (length 2.54)
        (name "DE" (effects (font (size 1.27 1.27))))
        (number "3" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -7.62 0 0) (length 2.54)
        (name "D" (effects (font (size 1.27 1.27))))
        (number "4" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 7.62 -6.35 180) (length 2.54)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "5" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 7.62 2.54 180) (length 2.54)
        (name "A" (effects (font (size 1.27 1.27))))
        (number "6" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 7.62 6.35 180) (length 2.54)
        (name "B" (effects (font (size 1.27 1.27))))
        (number "7" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -7.62 6.35 0) (length 2.54)
        (name "VCC" (effects (font (size 1.27 1.27))))
        (number "8" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 7.62 -3.81 180) (length 2.54)
        (name "EPAD" (effects (font (size 1.27 1.27))))
        (number "9" (effects (font (size 1.27 1.27))))
      )
    )
  )
)
